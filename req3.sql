SELECT r.reservation_id AS "Numero", c.last_name AS "Nom acheteur", c.first_name AS "Prenom acheteur", e.last_name AS "Nom employeur", e.first_name AS "Prenom employeur", r.creation_date AS "Date creation"
FROM olvoyage.t_reservation r
JOIN olvoyage.t_employee e
ON ( r.employee_id = e.employee_id )
JOIN olvoyage.t_customer c
ON ( c.customer_id = r.buyer_id )
WHERE r.creation_date IN
( SELECT MIN(creation_date)
FROM olvoyage.t_reservation);