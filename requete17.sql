UPDATE olvoyage.T_RESERVATION
SET buy_method='&moyen_paiement',
	price = (SELECT
				SUM(CASE
						WHEN TO_CHAR(tr.post_time,'D') IN (6,7)
						THEN tr.price - (tr.price*NVL(pass.discount_we_pct,0)/100)
							+ (tr.price*NVL(wag.class_pct,0)/100)
						WHEN TO_CHAR(tr.post_time,'D')=5 AND TO_CHAR(tr.post_time, 'hh24:mi')>'20:00'
						THEN tr.price - (tr.price*NVL(pass.discount_we_pct,0)/100)
							+ (tr.price*NVL(wag.class_pct,0)/100)
						ELSE tr.price - (tr.price*NVL(pass.discount_pct,0)/100)
							+ (tr.price*NVL(wag.class_pct,0)/100)
				END) "Prix total"
			FROM olvoyage.T_RESERVATION res
			JOIN olvoyage.T_TICKET tic
			ON (res.reservation_id = tic.reservation_id)
			JOIN olvoyage.T_CUSTOMER cus
			ON (tic.customer_id = cus.customer_id)
			JOIN olvoyage.T_WAGON_TRAIN twt
			ON (tic.wag_tr_id = twt.wag_tr_id)
			JOIN olvoyage.T_WAGON wag
			ON (twt.wagon_id = wag.wagon_id)
			JOIN olvoyage.T_TRAIN tr
			ON (twt.train_id = tr.train_id)
			LEFT JOIN olvoyage.T_PASS pass
			ON (cus.pass_id = pass.pass_id)
			WHERE res.reservation_id = &&reservation
			GROUP BY res.reservation_id)
WHERE reservation_id = &&reservation;