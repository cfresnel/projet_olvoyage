SELECT COUNT( DISTINCT res.reservation_id) "Rés. Séniors Janvier 2015"
FROM OLVOYAGE.T_TRAIN tr
    JOIN OLVOYAGE.T_WAGON_TRAIN twt
        ON tr.Train_id = twt.train_id
    JOIN OLVOYAGE.T_TICKET tic
        ON twt.wag_tr_id = tic.wag_tr_id
    JOIN OLVOYAGE.T_RESERVATION res
        ON res.reservation_id = tic.reservation_id
    JOIN OLVOYAGE.T_CUSTOMER cus 
        ON( cus.customer_id = res.buyer_id )
    JOIN OLVOYAGE.T_PASS pas
        ON( pas.pass_id = cus.pass_id )
WHERE LOWER(pas.pass_name) = 'senior'
 AND res.price IS NOT NULL
 AND TO_CHAR(tr.post_time, 'MM/YY') = '02/15';