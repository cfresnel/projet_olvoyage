SELECT NbE "Nombre d'employés",
       NbClt "Nombre d'acheteurs",
       PrctAbon "Pourcentage d'abonnés",
       NbRes "Nombre de réservations",
       NbTkt "Nombre de tickets", 
       NbTr "Nombre de train",
       NbSt "Nombre de station"
FROM ( SELECT COUNT( DISTINCT e.employee_id) NbE
       FROM olvoyage.t_employee e ) Employee,
     ( SELECT COUNT( DISTINCT c.customer_id) NbClt,
             ROUND((COUNT(c.pass_id) / COUNT( DISTINCT c.customer_id) * 100),2) || '%' PrctAbon
       FROM olvoyage.t_customer c ) ,
     ( SELECT COUNT( DISTINCT r.reservation_id) NbRes
       FROM olvoyage.t_reservation r ) ,
     ( SELECT COUNT( DISTINCT t.ticket_id ) NbTkt
       FROM olvoyage.t_ticket t ) ,
     ( SELECT COUNT( DISTINCT tr.train_id ) NbTr
       FROM olvoyage.t_train tr ) ,
     ( SELECT COUNT( DISTINCT st.station_id ) NbSt
       FROM olvoyage.t_station st ) 
       ;