SELECT pas.pass_name "Abonnement"
FROM OLVOYAGE.T_PASS pas
JOIN OLVOYAGE.T_CUSTOMER cus
ON ( pas.pass_id = cus.pass_id)
GROUP BY cus.pass_id, pas.pass_name
ORDER BY COUNT( cus.pass_id) DESC;