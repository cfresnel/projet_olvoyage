SELECT *
FROM 
		(SELECT e.first_name||' '||UPPER(e.last_name) "Prénom et NOM"
		FROM olvoyage.t_employee e 
		NATURAL JOIN olvoyage.t_reservation r
        GROUP BY e.last_name, e.first_name
        ORDER BY COUNT(r.reservation_id) DESC)
WHERE ROWNUM <=1
;