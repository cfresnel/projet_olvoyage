14) 
SELECT tr.train_id "Num�ro", st1.city ||' - ' || st2.city "Nom",
       Place.Somme - Ticket.Compte "Places libres"
FROM ( SELECT SUM(wa.nb_site) Somme, tr.train_id trainid
        FROM olvoyage.t_train tr
            JOIN olvoyage.t_wagon_train wt
                ON ( tr.train_id = wt.train_id )
            JOIN olvoyage.t_wagon wa
                ON ( wt.wagon_id = wa.wagon_id )
        GROUP BY tr.train_id) Place
    JOIN ( SELECT COUNT(ti.ticket_id) Compte, tr.train_id trainid2
           FROM olvoyage.t_train tr
             JOIN olvoyage.t_wagon_train wt
                ON tr.train_id = wt.train_id
             JOIN olvoyage.t_wagon wa
                ON ( wt.wagon_id = wa.wagon_id )
             JOIN olvoyage.t_ticket ti
                ON ( wt.wag_tr_id = ti.wag_tr_id )
            GROUP BY tr.train_id) Ticket
        ON ( Place.trainid = Ticket.trainid2 )
    JOIN olvoyage.t_train tr
        ON ( tr.train_id = Place.trainid)
    JOIN olvoyage.t_station st1
        ON ( st1.station_id = tr.departure_id )
    JOIN olvoyage.t_station st2
        ON ( st2.station_id = tr.arrival_id )
WHERE tr.distance > 300
    AND tr.arrival_time <= '22/01/15'
ORDER BY tr.train_id;
