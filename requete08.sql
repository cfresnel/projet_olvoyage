SELECT UPPER(c.last_name )||' '|| c.first_name "Nom et Prénom", c.address "Adresse"
FROM OLVOYAGE.T_CUSTOMER c
WHERE c.pass_id IS NULL
ORDER BY UPPER(c.last_name )||' '|| c.first_name;