SELECT UPPER(c.last_name )||' '|| c.first_name "Nom et Prénom",
CASE 
WHEN (MONTHS_BETWEEN('30/09/2015', c.pass_date ) > 12) THEN 'Périmé !'
WHEN (MONTHS_BETWEEN('30/09/2015', c.pass_date ) <= 12) THEN p.pass_name
WHEN (MONTHS_BETWEEN('30/09/2015', c.pass_date ) IS NULL) THEN 'Aucun'
END "Abonnnement"
FROM OLVOYAGE.T_CUSTOMER c
LEFT OUTER JOIN OLVOYAGE.T_PASS p
ON ( p.pass_id = c.pass_id )
ORDER BY UPPER(c.last_name )||' '|| c.first_name;