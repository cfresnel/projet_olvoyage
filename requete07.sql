SELECT *
FROM(SELECT stDep.city ||' - '|| stArr.city "Top-5 des trains"
        FROM OLVOYAGE.T_TRAIN tr
        JOIN OLVOYAGE.T_STATION stDep
            ON tr.departure_id = stDep.station_id
        JOIN OLVOYAGE.T_STATION stArr
            ON tr.arrival_id = stArr.station_id
        JOIN OLVOYAGE.T_WAGON_TRAIN twt
            ON tr.Train_id = twt.train_id
        JOIN OLVOYAGE.T_TICKET tic
            ON twt.wag_tr_id = tic.wag_tr_id
        GROUP BY stDep.city ||' - '|| stArr.city, train_id
        ORDER BY COUNT(tic.TICKET_ID) DESC )
WHERE ROWNUM <=5;