SELECT t.train_id, d.city || ' (' || TO_CHAR(t.post_time, 'DD/MM/YY HH24:MI:SS') || ') - ' || r.city || ' (' || TO_CHAR(t.arrival_time, 'DD/MM/YY HH24:MI:SS') || ')', t.distance, t.price
FROM olvoyage.T_TRAIN t
JOIN olvoyage.T_STATION d
ON (t.departure_id = d.station_id)
JOIN olvoyage.T_STATION r
ON (t.arrival_id = r.station_id)
ORDER BY t.train_id;