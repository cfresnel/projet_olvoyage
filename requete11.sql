SELECT tr.train_id "‪#‎Train‬", tr.price "Prix",pass_name "Abonnemment",
		ROUND(tr.price - (tr.price*(pas.discount_pct/100)),2) || ' $' "Réduction en semaine",
		ROUND(tr.price - (tr.price*(pas.discount_we_pct/100)),2) || ' $' "Réduction en week-end",
		stdep.city ||' - ' || stArr.city "Nom du train"
FROM OLVOYAGE.T_TRAIN tr
JOIN OLVOYAGE.T_STATION stDep
ON tr.departure_id = stDep.station_id
JOIN OLVOYAGE.T_STATION stArr
ON tr.arrival_id = stArr.station_id
CROSS JOIN OLVOYAGE.T_PASS pas
WHERE stDep.city='Paris'
ORDER BY tr.train_id;