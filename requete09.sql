SELECT tr.train_id "Numéro du Train",
stDep.city ||' - '|| stArr.city "Nom du Train",
ROUND(DISTANCE/((tr.arrival_time - tr.post_time)*24),0) || 'km/h' "Vitesse moyenne",
FROM olvoyage.t_train tr
  JOIN olvoyage.t_station stDep
  ON tr.departure_id = stDep.station_id
  JOIN olvoyage.t_station stArr
  ON tr.arrival_id = stArr.station_id;
