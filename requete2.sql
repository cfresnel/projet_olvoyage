SELECT DISTINCT c.last_name, c.first_name
FROM olvoyage.T_RESERVATION r
JOIN olvoyage.T_CUSTOMER c
ON (r.buyer_id = c.customer_id)
JOIN olvoyage.T_TICKET t
ON (t.reservation_id = r.reservation_id)
WHERE r.reservation_id
IN (SELECT r.reservation_id
FROM olvoyage.T_RESERVATION r
JOIN olvoyage.T_CUSTOMER c
ON (r.buyer_id = c.customer_id)
JOIN olvoyage.T_TICKET t
ON (t.reservation_id = r.reservation_id)
GROUP BY r.reservation_id
HAVING COUNT(DECODE(t.customer_id, r.buyer_id, 1, NULL)) = 0)
ORDER BY c.last_name, c.first_name;