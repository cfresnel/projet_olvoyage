SELECT tkt.ticket_id, c.first_name||' '|| UPPER(c.last_name ) "Prénom et NOM du client",
       stdep.city ||' - ' || stArr.city "Nom du train"
FROM olvoyage.t_train tr
    JOIN olvoyage.T_station stDep
        ON tr.departure_id = stDep.station_id
    JOIN olvoyage.T_station stArr
        ON tr.arrival_id = stArr.station_id
    JOIN olvoyage.T_wagon_train wagtr
        ON tr.Train_id = wagtr.train_id
    JOIN olvoyage.T_wagon wag
        ON wagtr.wagon_id = wag.wagon_id
    JOIN olvoyage.T_ticket tkt
        ON wagtr.wag_tr_id = tkt.wag_tr_id
    JOIN olvoyage.T_reservation r
        ON tkt.reservation_id = r.reservation_id 
    JOIN olvoyage.T_customer c
        ON c.customer_id = r.buyer_id 
WHERE wag.wagon_id IN (1,2)
    AND MONTHS_BETWEEN(sysdate,c.birth_date) < (25*12)
    AND r.creation_date <= (tr.post_time - 20) 
    AND TO_CHAR(tr.post_time, 'DD/MM/YYYY')  BETWEEN '15/04/2015' AND '25/04/2015'
ORDER BY r.creation_date
;