SELECT Reservations, LPAD(employes, LENGTH(employes)+(LEVEL*2)-2,' ') "Employes"
FROM (SELECT UPPER(e.last_name)||' '||e.first_name "employes", COUNT(r.reservation_id) Reservations, employee_id, e.manager_id
FROM olvoyage.T_RESERVATION r
RIGHT OUTER JOIN olvoyage.T_EMPLOYEE e
USING (employee_id)
GROUP BY UPPER(e.last_name)||' '||e.first_name, employee_id, e.manager_id)
START WITH employee_id IN
(SELECT employee_id
FROM olvoyage.T_EMPLOYEE
WHERE manager_id=1)
CONNECT BY PRIOR employee_id = manager_id;